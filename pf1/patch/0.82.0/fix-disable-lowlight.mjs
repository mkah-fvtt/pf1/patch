// 0.82.0 – 0.82.2

function getRadiusLLVFix(wrapped) {
	if (this.object?.document?.getFlag('pf1', 'disableLowLight')) return { dim: this.data.dim, bright: this.data.bright };
	return wrapped.call(this);
}

const fixDisableLowLight = () => {
	if (game.release.generation >= 10) {
		if (isNewerVersion(game.system.version, '0.82.2')) return;
		console.log('PF1 PATCH 🩹 | Fxing light source disable low-light toggle: https://gitlab.com/foundryvtt_pathfinder1e/foundryvtt-pathfinder1/-/issues/1736')
		libWrapper.register('pf1-patch', 'LightSource.prototype.getRadius', getRadiusLLVFix, libWrapper.MIXED);
	}
}

Hooks.once('init', fixDisableLowLight);
