// 0.82.1

function fixRollPF1(wrapped, data, ...args) {
	if (data.class === 'RollPF$1') data.class = 'RollPF';
	return wrapped(data, ...args);
}

Hooks.once('init', () => {
	const version = game.system.version ?? game.system.data.version;
	if (version !== '0.82.1') return;

	console.log('PF1 PATCH 🩹 | Overloading Roll.fromData to mitigate RollPF$1 errors')
	libWrapper.register('pf1-patch', 'Roll.fromData', fixRollPF1, libWrapper.WRAPPER);
});
