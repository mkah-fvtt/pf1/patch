console.log('PF1 PATCH 🩹 | cost of undefined in getDefaultChargeFormula() with spellpoints enabled');

function fixGetDefaultChargeFormula(wrapped) {
	if (this.useSpellPoints()) {
		return this.system.spellPoints?.cost || '0';
	}
	else {
		return pf1.documents.item.ItemPF.prototype.getDefaultChargeFormula.call(this);
	}
}

libWrapper.register('pf1-patch', 'pf1.documents.item.ItemSpellPF.prototype.getDefaultChargeFormula', fixGetDefaultChargeFormula, libWrapper.OVERRIDE);
