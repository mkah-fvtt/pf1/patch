// https://gitlab.com/foundryvtt_pathfinder1e/foundryvtt-pathfinder1/-/issues/1406
// https://gitlab.com/foundryvtt_pathfinder1e/foundryvtt-pathfinder1/-/issues/1407

console.log('PF1 PATCH 🩹 | Temp HP bug: https://gitlab.com/Furyspark/foundryvtt-pathfinder1/-/issues/1407');

console.log('PF1 PATCH 🩹 | Health tracking module incompatibility: https://gitlab.com/Furyspark/foundryvtt-pathfinder1/-/issues/1406');

// const orig = game.pf1.documents.ActorPF.prototype.updateDocuments;
game.pf1.documents.ActorPF.updateDocuments = function (...args) {
	return CONFIG.Actor.documentClass.updateDocuments.call(this, ...args);
}
