// https://gitlab.com/foundryvtt_pathfinder1e/foundryvtt-pathfinder1/-/issues/1399

{
	console.log('PF1 PATCH 🩹 | Proficiency & languages errors: https://gitlab.com/foundryvtt_pathfinder1e/foundryvtt-pathfinder1/-/issues/1399');

	const orig = game.pf1.documents.ActorPF.prototype.prepareProficiencies;
	game.pf1.documents.ActorPF.prototype.prepareProficiencies = function (...args) {
		orig.call(this, ...args);
		['armorProf', 'weaponProf', 'languages'].forEach(p => {
			this.data.data.traits[p].custom ??= '';
			this.data.data.traits[p].value ?? [];
		})
	}
}
