console.log('PF1 PATCH 🩹 | Attempting to mitigate damage');
console.error('!!! PLEASE INSTALL DIFFERENT VERSION OF PATHFINDER 1 !!!');

const warningMessage = () => {
	ui.notifications.error('PF1 v0.80.19 is harmful to your data, please install different version and restore backups if necessary.', { permanent: true });
	console.error('!!! PLEASE INSTALL DIFFERENT VERSION OF PATHFINDER 1 !!!');
	return false;
};

Hooks.on('preUpdateActor', () => warningMessage());
Hooks.on('preUpdateItem', () => warningMessage());

const disableMigration = () => game.pf1.migrateWorld = () => { };

// Aggressively attempt to disable migration functionality
Hooks.once('init', () => disableMigration());
Hooks.once('setup', () => disableMigration());
Hooks.once('ready', () => disableMigration());

Hooks.once('ready', () => warningMessage());
