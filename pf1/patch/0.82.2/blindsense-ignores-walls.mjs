// 0.82.0 – 0.82.2

Hooks.once('pf1PostInit', () => {
	if ((game.release?.generation ?? 8) < 10) return;
	if (isNewerVersion(game.system.version, '0.82.2')) return;

	console.log('PF1 PATCH 🩹 | Blindsense/Blindsight ignores walls')

	CONFIG.Canvas.detectionModes.blindSense.walls = true;
	CONFIG.Canvas.detectionModes.blindSight.walls = true;
});
