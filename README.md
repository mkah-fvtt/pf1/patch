# Patch for PF1

[![Latest Release](https://gitlab.com/mkah-fvtt/pf1/patch/-/badges/release.svg)](https://gitlab.com/mkah-fvtt/pf1/patch/-/releases)

Provides temporary quick patching for PF1.

See [change log](./CHANGELOG.md) for what fixes are applied to each version, the versions match PF1 major versions they apply to.

The first 3 numbers relate to PF1 versions, fourth number is used to signify patches and iterations to this module.

## Install

Manifest URL: <https://gitlab.com/mkah-fvtt/pf1/patch/-/releases/permalink/latest/downloads/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
