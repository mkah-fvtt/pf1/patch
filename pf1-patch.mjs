console.log('PF1 PATCH 🩹 | Initializing');

// Hack needed to get overrides for migration or similar early states
import './pf1/patch/0.76.7/fix-item-range-migration.mjs';
import './pf1/patch/0.80.20/bad-rolldata-shield-handling.mjs';
import './pf1/patch/0.80.22/fix-vision-update-for-actorless.mjs';
import './pf1/patch/0.80.23/new-senses-with-basic-actor.mjs';
import './pf1/patch/0.82.0/rolldata-fcb.mjs';
import './pf1/patch/0.82.0/fix-disable-lowlight.mjs';
import './pf1/patch/0.82.1/roll-fromdata.mjs';
import './pf1/patch/0.82.2/fix-vision.mjs';
import './pf1/patch/0.82.2/blindsense-ignores-walls.mjs';
import './pf1/patch/0.82.2/stacking-bonuses.mjs';

Hooks.once('init', function patchInit() {
	const version = game.system.version ?? game.system.data.version;
	import(`./pf1/${version}.mjs`)
		.catch(err => console.warn(`PF1 PATCH 🩹 | Not available for PF1 version ${version}`, err));
});
